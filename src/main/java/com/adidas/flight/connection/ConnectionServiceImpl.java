package com.adidas.flight.connection;

import java.util.List;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class ConnectionServiceImpl implements ConnectionService {

	private final ConnectionRepository repository;

	@Override
	public List<Connection> getConnectionsFromCity(String originCity) {
		List<Connection> result = repository.findByOriginCityIgnoreCase(originCity);

		if (result.isEmpty()) {
			throw new ConnectionNotFound(String.format("There is not any connection from city '%s'", originCity));
		}

		return result;
	}

}
