package com.adidas.flight.connection;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

interface ConnectionRepository extends JpaRepository<Connection, Integer> {

	List<Connection> findByOriginCityIgnoreCase(String originCity);

}
