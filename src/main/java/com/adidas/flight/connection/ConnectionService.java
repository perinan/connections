package com.adidas.flight.connection;

import java.util.List;

interface ConnectionService {

	List<Connection> getConnectionsFromCity(String fromCity);
	
}
