package com.adidas.flight.connection;

import java.time.LocalTime;

import lombok.Data;

@Data
class ConnectionDto {
	
	private String originCity;
	private String destinyCity;
	private LocalTime departureTime;
	private LocalTime arrivalTime;

}
