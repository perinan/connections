package com.adidas.flight.connection;

import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
class Connection {

	@Id
	private Integer id;
	
	private String originCity;
	private String destinyCity;
	private LocalTime departureTime;
	private LocalTime arrivalTime;
	
}
