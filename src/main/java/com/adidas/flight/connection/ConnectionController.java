package com.adidas.flight.connection;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/connections")
@RequiredArgsConstructor
@Slf4j
class ConnectionController {

	private final ConnectionService service;

	private final ModelMapper modelMapper;

	@GetMapping("/from/{fromCity}")
	List<ConnectionDto> getConnections(@PathVariable(name = "fromCity", required = true) final String fromCity) {
		log.info(String.format("Connections from %s",
				fromCity));

		List<Connection> connections = service.getConnectionsFromCity(fromCity);

		return connections.stream()
			.map(domain -> modelMapper.map(domain,
					ConnectionDto.class))
			.collect(Collectors.toList());
	}

	@ExceptionHandler(ConnectionNotFound.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String noConnections(ConnectionNotFound exception) {
		log.error("Connection not found",
				exception);

		return exception.getMessage();
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String error(Exception exception) {
		log.error("Error",
				exception);

		return exception.getMessage();
	}
}
