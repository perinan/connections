package com.adidas.flight.connection;

class ConnectionNotFound extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ConnectionNotFound (String message) {
		super(message);
	}
}
