insert into connection (id, origin_city, destiny_city, departure_time, arrival_time) 
	values (1, 'Seville', 'Madrid', '09:00:00', '10:05:00');
insert into connection (id, origin_city, destiny_city, departure_time, arrival_time) 
	values (2, 'Seville', 'Barcelona', '09:00:00', '10:50:00');
insert into connection (id, origin_city, destiny_city, departure_time, arrival_time) 
	values (3, 'Madrid', 'London', '12:00:00', '14:50:00');
insert into connection (id, origin_city, destiny_city, departure_time, arrival_time) 
	values (4, 'London', 'Helsinki', '10:00:00', '13:20:00');
insert into connection (id, origin_city, destiny_city, departure_time, arrival_time) 
	values (5, 'Barcelona', 'Helsinki', '09:30:00', '13:35:00');