# Project Title

API in order to expose the data related with a city defined with: city, destiny city, departure time, arrival time.

## Getting Started

You can get a copy of the project with the following command:
 ```git clone https://bitbucket.org/perinan/connections.git```

### Prerequisites

Your machine has to have the following tools installed:
 - Maven
 - VM Java 8

### Build

You can build the project with the following command:
 ```mvn package```

## Run

You can run the microservice executing:
 ```mvn spring-boot:run```

## Use

You should open a browser and write the url ```http://localhost:8081/connections/from/{fromCity}```, where fromCity
is the origin city of the journey.

Example:
	```http://localhost:8081/connections/from/Seville```

## Documentation

The documentation is a swagger site which is available in ```http://localhost:8081/swagger-ui.html```

## Frameworks and libraries used

This microservice has been developed using the following frameworks and libraries:
 - Java 8
 - Spring Boot.
 - Spring Data JPA.
 - Swagger.
 - Lombok.
